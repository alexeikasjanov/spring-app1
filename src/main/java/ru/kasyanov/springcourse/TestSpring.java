package ru.kasyanov.springcourse;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kasyanov.springcourse.config.SpringConfig;


public class TestSpring {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                SpringConfig.class
        );

        MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

        System.out.println(musicPlayer.playMusic());

        context.close();
    }
}

//   Computer computer = context.getBean("computer", Computer.class);
// System.out.println(computer);
// MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class); // musicPlayer - название класса с маленькой буквы
// musicPlayer.playMusic();
// Music music = context.getBean("rockMusic", Music.class);
// MusicPlayer musicPlayer = new MusicPlayer(music);
// musicPlayer.playMusic();
// Music music2 = context.getBean("classicalMusic", Music.class);
// MusicPlayer musicPlayer2 = new MusicPlayer(music2);
//musicPlayer2.playMusic();
//ClassicalMusic classicalMusic = context.getBean("musicBean",ClassicalMusic.class);
//System.out.println(classicalMusic.getSong());
//Music music = context.getBean("musicBean",Music.class);
// теперь совершим внедрение зависимости (вручную)!
//MusicPlayer musicPlayer = new MusicPlayer(music); // получили зависимость из spring контекста
// и внедрили его в наш конструктор нашего музыкального плеера
// это все вручную
// MusicPlayer firstMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class); // это через контекст
//MusicPlayer secondMusicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
// boolean c = firstMusicPlayer == secondMusicPlayer;
// System.out.println(firstMusicPlayer);
//  System.out.println(secondMusicPlayer);
//  firstMusicPlayer.setVolume(10);
//  System.out.println(firstMusicPlayer.getVolume());
//  System.out.println(secondMusicPlayer.getVolume() );
// System.out.println(c);

// считывает его и помещает все бины внутри него в application context
// ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
//       "applicationContext.xml" // обязательно в папке помеченой как ресурсы
//);
