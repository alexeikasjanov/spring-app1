package ru.kasyanov.springcourse.genres;

import ru.kasyanov.springcourse.Music;

public class RockMusic implements Music {
    @Override
    public String getSong() {
        return "Wind cries Mary";
    }
}