package ru.kasyanov.springcourse.genres;

import ru.kasyanov.springcourse.Music;

public class JazzMusic implements Music {
    @Override
    public String getSong() {
        return "Take five";
    }
}