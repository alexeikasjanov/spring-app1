package ru.kasyanov.springcourse.genres;
import ru.kasyanov.springcourse.Music;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class ClassicalMusic implements Music {

    @PostConstruct
    public void doMyInit() {
        System.out.println("Doing my initialization");
    }

    // Для Prototype бинов не вызывается destroy-метод!
    @PreDestroy
    public void doMyDestroy() {
        System.out.println("Doing my destruction");
    }

    @Override
    public String getSong() {
        return "Hungarian Rhapsody";
    }
}

//private ClassicalMusic()
// public static ClassicalMusic getClassicalMusic(){
// return  new ClassicalMusic();
//}
///public void doMyInit(){
//  System.out.println("do my init");
// }
//public void doMyDestroy(){
//   System.out.println("do my destroy");
// }