package ru.kasyanov.springcourse;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.Random;


public class MusicPlayer {

    @Value("${musicPlayer.name}")
    private String name;

    @Value("${musicPlayer.volume}")
    private int volume;

    private List<Music> musicList;

    public MusicPlayer(List<Music> musicList) {
        this.musicList = musicList;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    public String playMusic() {
        Random random = new Random();

        return "Playing: " + musicList.get(random.nextInt(musicList.size())).getSong()
                + " with volume " + this.volume;
    }
}

//  private String name;
// private int volume;
// public String getName() {
//    return name;
// }
// public void setName(String name) {
//    this.name = name;
/// }
//  public int getVolume() {
//     return volume;
// }
// public void setVolume(int volume) {
//    this.volume = volume;
// }
// public MusicPlayer(){}
// IoC инверсия управления
//public void setMusic(Music music){
//  this.music = music;
// }